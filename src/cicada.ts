export
class id_t {}

export
class name_t {
  constructor (
    public str: string,
  ) {}
}

export
class body_t {
  constructor (
    public map: Map <name_t, obj_t> = new Map ()
  ) {}
}

export
abstract class exp_t {}

export
abstract class obj_t {}

export
class var_obj_t
extends obj_t {
  constructor (
    public id: id_t,
    public name: name_t,
    public t: obj_t,
  ) {
    super ()
  }
}

export
class disj_obj_t
extends obj_t {
  constructor (
    public name: name_t,
    public sub_type_names: Array <name_t>,
    public body: body_t,
  ) {
    super ()
  }
}

export
class conj_obj_t
extends obj_t {
  constructor (
    public name: name_t,
    public body: body_t,
  ) {
    super ()
  }
}

export
class data_obj_t
extends obj_t {
  constructor  (
    public name: name_t,
    public body: body_t,
  ) {
    super ()
  }
}

export
class tt_obj_t
extends obj_t {}

export
class module_obj_t
extends obj_t {
  constructor (
    public body: body_t = new body_t (),
  ) {
    super ()
  }
}
